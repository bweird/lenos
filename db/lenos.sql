/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : lenos

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-01-02 16:55:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `p_id` varchar(36) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `order_num` int(4) DEFAULT NULL COMMENT '排序字段',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `create_by` varchar(32) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL COMMENT '权限',
  `menu_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1栏目2菜单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('3873ccc2dfda11e7b555201a068c6482', '菜单管理', 'cfda8029dfb311e7b555201a068c6482', 'menu/showMenu', '1', '&#xe62a;', null, '2017-12-14 14:02:50', null, '2017-12-14 14:02:50', null, '0');
INSERT INTO `sys_menu` VALUES ('433089a6eb0111e782d5201a068c6482', '编辑', 'cfe54921dfb311e7b555201a068c6482', '', null, '1', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-27 20:27:11', null, null, 'user:update', '1');
INSERT INTO `sys_menu` VALUES ('6dc13c6eec5f11e7a472201a068c6482', '系统日志', 'a1ca6642ec5e11e7a472201a068c6482', 'log/showLog', '1', '&#xe60a;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-29 14:43:46', null, '2017-12-29 14:43:46', null, '0');
INSERT INTO `sys_menu` VALUES ('7967e098ee0611e7a60d201a068c6482', '接口api', 'a1ca6642ec5e11e7a472201a068c6482', 'swagger-ui.html', '2', '&#xe64e;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-31 16:42:04', null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('a1ca6642ec5e11e7a472201a068c6482', '系统监控', null, null, '2', '&#xe62c;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-29 14:52:14', null, '2017-12-29 14:52:14', null, '0');
INSERT INTO `sys_menu` VALUES ('b441914cee0811e7a60d201a068c6482', '系统监控', 'a1ca6642ec5e11e7a472201a068c6482', 'druid/index.html', '3', '&#xe628;', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-31 16:58:01', null, null, null, '0');
INSERT INTO `sys_menu` VALUES ('cfda8029dfb311e7b555201a068c6482', '系统管理', null, null, '1', '&#xe614;', null, '2017-12-29 14:59:22', null, '2017-12-29 14:59:22', null, '0');
INSERT INTO `sys_menu` VALUES ('cfe54921dfb311e7b555201a068c6482', '用户管理', 'cfda8029dfb311e7b555201a068c6482', 'user/showUser', '2', '&#xe6af;', null, '2017-12-29 14:40:34', null, '2017-12-29 14:40:34', null, '0');
INSERT INTO `sys_menu` VALUES ('cfe54921dfb311e7b555201a068c6483', '增加', 'cfe54921dfb311e7b555201a068c6482', null, '1', null, null, null, null, null, 'user:select', '1');
INSERT INTO `sys_menu` VALUES ('cff61424dfb311e7b555201a068c6482', '角色管理', 'cfda8029dfb311e7b555201a068c6482', 'role/showRole', '3', '&#xe613;', null, '2017-12-29 14:40:36', null, '2017-12-29 14:40:36', null, '0');
INSERT INTO `sys_menu` VALUES ('e3b11497eb9e11e7928d201a068c6482', '删除', 'cfe54921dfb311e7b555201a068c6482', '', null, '', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-28 15:17:07', null, '2017-12-28 15:17:07', 'user:del', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL,
  `role_name` varchar(128) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('0ea934e5e55411e7b983201a068c6482', 'group1', '一组', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-20 15:04:44', null, '2018-01-02 11:41:43');
INSERT INTO `sys_role` VALUES ('2619a672e53811e7b983201a068c6482', 'admin', '管理员', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-20 11:44:57', null, '2018-01-02 11:38:37');
INSERT INTO `sys_role` VALUES ('576e0c81eb1311e782d5201a068c6482', 'group2', '二组', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-27 22:36:36', null, null);
INSERT INTO `sys_role` VALUES ('a56219ffeb7d11e7928d201a068c6482', 'g10', '十组', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-28 11:17:33', null, null);
INSERT INTO `sys_role` VALUES ('b9fb1173eb7611e7928d201a068c6482', 'group3', '三组', 'acfc0e9232f54732a5d9ffe9071bf572', '2017-12-28 10:28:01', null, null);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` varchar(32) NOT NULL,
  `menu_id` varchar(32) NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', '6dc13c6eec5f11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'a1ca6642ec5e11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'cfe54921dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('0ea934e5e55411e7b983201a068c6482', 'e3b11497eb9e11e7928d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '3873ccc2dfda11e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '433089a6eb0111e782d5201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '6dc13c6eec5f11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', '7967e098ee0611e7a60d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'a1ca6642ec5e11e7a472201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'b441914cee0811e7a60d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cfe54921dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cfe54921dfb311e7b555201a068c6483');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'cff61424dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('2619a672e53811e7b983201a068c6482', 'e3b11497eb9e11e7928d201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('a56219ffeb7d11e7928d201a068c6482', '433089a6eb0111e782d5201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('a56219ffeb7d11e7928d201a068c6482', 'cfda8029dfb311e7b555201a068c6482');
INSERT INTO `sys_role_menu` VALUES ('a56219ffeb7d11e7928d201a068c6482', 'cfe54921dfb311e7b555201a068c6482');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `user_id` varchar(32) NOT NULL,
  `role_id` varchar(32) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('2211fec3e17c11e795ed201a068c6482', '0ea934e5e55411e7b983201a068c6482');
INSERT INTO `sys_role_user` VALUES ('7dfe1191eaa911e79999201a068c6482', '0ea934e5e55411e7b983201a068c6482');
INSERT INTO `sys_role_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', '0ea934e5e55411e7b983201a068c6482');
INSERT INTO `sys_role_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', '2619a672e53811e7b983201a068c6482');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(36) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(128) NOT NULL,
  `age` int(4) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `real_name` varchar(18) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0可用1封禁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('2211fec3e17c11e795ed201a068c6482', 'zxm', 'f8880ebbdbc37a936245657fa9084198', '24', '154040976@qq.com', '', '', null, null, '2017-12-15 17:41:32', '2017-12-20 16:33:29', '0');
INSERT INTO `sys_user` VALUES ('7dfe1191eaa911e79999201a068c6482', 'ceshi', 'e6e7d1d292662f8a30a3f2c57049af00', '24', '', '', '', null, null, '2017-12-27 09:58:54', null, '0');
INSERT INTO `sys_user` VALUES ('900ac744db0111e7b309201a068c6482', 'zhangsan', '9bad41710724cg6511abde2a52416881', '24', '154040976@qq.com', null, null, null, null, '2017-12-18 16:38:08', '2017-12-18 16:38:08', '1');
INSERT INTO `sys_user` VALUES ('91b1af44db1511e7b309201a068c6482', 'zhang18', 'b67bdb1e035545aeg7eg4ab85a8g5dd0', '24', null, null, null, null, null, '2017-12-18 16:38:20', '2017-12-18 16:38:20', '1');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf510', 'ceshi5', '123456', '24', '154040976@qq.com', null, null, null, null, '2017-12-18 16:40:20', '2017-12-18 16:40:20', '1');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf513', 'ceshi9', '123457', '24', '154040976@qq.com', null, null, null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf514', 'ceshi10', '123457', '24', '154040976@qq.com', null, null, null, null, '2017-12-18 20:22:10', '2017-12-18 20:22:10', '1');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf572', 'admin', 'e0b141de1c8091be350d3fc80de66528', '24', '', 'image/2018-01-02/04112710081355948a3f0cf5-464x700.jpg', '', null, null, '2017-12-20 16:34:06', '2017-12-20 16:34:06', '0');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf573', 'ceshi1', '123456', '24', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf574', 'ceshi2', '123456', '24', '154040976@qq.com', null, null, null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('acfc0e9232f54732a5d9ffe9071bf577', 'ceshi3', '123456', '24', '154040976@qq.com', null, null, null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('b25094d9e39511e7b012201a068c6482', 'zhangsan1', '6bea0fa0327d690c26382a992342179c', '24', '154040976@qq.com', 'image/2017-12-18/094914111.jpg', '张三', null, null, '2017-12-18 09:49:33', null, '0');
INSERT INTO `sys_user` VALUES ('b4fe2048db1211e7b309201a068c6482', 'zhang2', '308ggbdd4g58d783bd5077cg1da31adb', '24', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('beeef6eadb1411e7b309201a068c6482', 'zhang11', '4g5c9cae6dd3gb0a2046a8bgbb7ed98b', '24', null, null, null, null, null, null, null, '0');
INSERT INTO `sys_user` VALUES ('ccb3774ddb1211e7b309201a068c6482', 'zhang3', '320725ca60cg5101e7bd719e9b3a176g', '24', null, null, null, null, null, null, null, '0');
